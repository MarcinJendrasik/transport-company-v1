<?php


namespace App\DataFixtures;

use App\Entity\Courier;
use App\Entity\District;
use App\Entity\User;
use App\Service\PasswordEncoderService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CourierFixtures extends Fixture
{

    private $passwordEncoderService;

    public function __construct(PasswordEncoderService $passwordEncoderService)
    {
        $this->passwordEncoderService = $passwordEncoderService;
    }

    public function load(ObjectManager $manager)
    {
        $this->createCourierFromBemowo($manager);
        $this->createCourierFromBialoleka($manager);
        $this->createCourierFromBielany($manager);
        $this->createCourierFromMokotow($manager);
        $this->createCourierFromOchota($manager);
        $this->createCourierFromPragaPoludnie($manager);
        $this->createCourierFromPragaPolnoc($manager);
        $this->createCourierFromRembertow($manager);
        $this->createCourierFromSrodmiescie($manager);
        $this->createCourierFromTargowek($manager);
        $this->createCourierFromUrsus($manager);
        $this->createCourierFromUrsynow($manager);
        $this->createCourierFromWawer($manager);
        $this->createCourierFromWesola($manager);
        $this->createCourierFromWilanow($manager);
        $this->createCourierFromWlochy($manager);
        $this->createCourierFromWola($manager);
        $this->createCourierFromZoliborz($manager);
    }

    /**
     * @param ObjectManager $manager
     */
    public function createCourierFromBemowo(ObjectManager $manager): void
    {
        $user = new User();
        $user->setName('Courier Bemowo');
        $user->setSurname('Bemowo');
        $user->setRole('ROLE_COURIER');
        $user->setPassword('bemowo');
        $user->setConfirmPassword('bemowo');
        $this->passwordEncoderService->encodePassword($user);
        $user->setEmail('bemowo@courier.com');

        $district = new District();
        $district->setName('Bemowo');

        $courier = new Courier();   //
        $courier->setUser($user);
        $courier->setDistrict($district);

        $manager->persist($courier);
        $manager->flush();
    }

    public function createCourierFromBialoleka(ObjectManager $manager): void
    {
        $user = new User();
        $user->setName('Courier Białołęka');
        $user->setSurname('Bialoleka');
        $user->setRole('ROLE_COURIER');
        $user->setPassword('bialoleka');
        $user->setConfirmPassword('bialoleka');
        $this->passwordEncoderService->encodePassword($user);
        $user->setEmail('bialoleka@courier.com');

        $district = new District();
        $district->setName('Białołęka');

        $courier = new Courier();   //
        $courier->setUser($user);
        $courier->setDistrict($district);

        $manager->persist($courier);
        $manager->flush();
    }

    public function createCourierFromBielany(ObjectManager $manager): void
    {
        $user = new User();
        $user->setName('Courier Bielany');
        $user->setSurname('Bielany');
        $user->setRole('ROLE_COURIER');
        $user->setPassword('bielany');
        $user->setConfirmPassword('bielany');
        $this->passwordEncoderService->encodePassword($user);
        $user->setEmail('bielany@courier.com');

        $district = new District();
        $district->setName('Bielany');

        // 10 kurierow - 1 na kazda dzielnice
        $courier = new Courier();   //
        $courier->setUser($user);
        $courier->setDistrict($district);

        $manager->persist($courier);
        $manager->flush();
    }


    //kurierzy sa tworzeni po to aby uzytkownicy mogli tworzyc zamowienia dla odpowiednich dzielnic

    public function createCourierFromMokotow(ObjectManager $manager): void
    {
        $user = new User();
        $user-> setName('Courier Mokotów');
        $user->setSurname('Mokotow');
        $user->setRole('ROLE_COURIER');
        $user->setPassword('mokotow');
        $user->setConfirmPassword('mokotow');
        $this->passwordEncoderService->encodePassword($user);
        $user->setEmail('mokotow@courier.com');

        $district = new District();
        $district->setName('Mokotów');

        $courier = new Courier();
        $courier->setUser($user);
        $courier->setDistrict($district);

        $manager->persist($courier);
        $manager->flush();
   }

    public function createCourierFromOchota(ObjectManager $manager): void
    {
        $user = new User();
        $user-> setName('Courier Ochota');
        $user->setSurname('Ochota');
        $user->setRole('ROLE_COURIER');
        $user->setPassword('ochota');
        $user->setConfirmPassword('ochota');
        $this->passwordEncoderService->encodePassword($user);
        $user->setEmail('ochota@courier.com');

        $district = new District();
        $district->setName('Ochota');

        $courier = new Courier();
        $courier->setUser($user);
        $courier->setDistrict($district);

        $manager->persist($courier);
        $manager->flush();
    }

    public function createCourierFromPragaPoludnie(ObjectManager $manager): void
    {
        $user = new User();
        $user-> setName('Courier Praga Południe');
        $user->setSurname('Praga Poludnie');
        $user->setRole('ROLE_COURIER');
        $user->setPassword('pragapld');
        $user->setConfirmPassword('pragapld');
        $this->passwordEncoderService->encodePassword($user);
        $user->setEmail('praga.pld@courier.com');

        $district = new District();
        $district->setName('Praga Południe');

        $courier = new Courier();
        $courier->setUser($user);
        $courier->setDistrict($district);

        $manager->persist($courier);
        $manager->flush();
    }

    public function createCourierFromPragaPolnoc(ObjectManager $manager): void
    {
        $user = new User();
        $user-> setName('Courier Praga Północ');
        $user->setSurname('Praga Polnoc');
        $user->setRole('ROLE_COURIER');
        $user->setPassword('pragapln');
        $user->setConfirmPassword('pragapln');
        $this->passwordEncoderService->encodePassword($user);
        $user->setEmail('praga.pln@courier.com');

        $district = new District();
        $district->setName('Praga Północ');

        $courier = new Courier();
        $courier->setUser($user);
        $courier->setDistrict($district);

        $manager->persist($courier);
        $manager->flush();
    }

    public function createCourierFromRembertow(ObjectManager $manager): void
    {
        $user = new User();
        $user-> setName('Courier Rembertów');
        $user->setSurname('Rembertow');
        $user->setRole('ROLE_COURIER');
        $user->setPassword('rembertow');
        $user->setConfirmPassword('rembertow');
        $this->passwordEncoderService->encodePassword($user);
        $user->setEmail('rembertow@courier.com');

        $district = new District();
        $district->setName('Rembertów');

        $courier = new Courier();
        $courier->setUser($user);
        $courier->setDistrict($district);

        $manager->persist($courier);
        $manager->flush();
    }

    public function createCourierFromSrodmiescie(ObjectManager $manager): void
    {
        $user = new User();
        $user-> setName('Courier Śródmieście');
        $user->setSurname('Srodmiescie');
        $user->setRole('ROLE_COURIER');
        $user->setPassword('srodmiescie');
        $user->setConfirmPassword('srodmiescie');
        $this->passwordEncoderService->encodePassword($user);
        $user->setEmail('srodmiescie@courier.com');

        $district = new District();
        $district->setName('Sródmieście');

        $courier = new Courier();
        $courier->setUser($user);
        $courier->setDistrict($district);

        $manager->persist($courier);
        $manager->flush();
    }

    public function createCourierFromTargowek(ObjectManager $manager): void
    {
        $user = new User();
        $user-> setName('Courier Targówek');
        $user->setSurname('Targowek');
        $user->setRole('ROLE_COURIER');
        $user->setPassword('targowek');
        $user->setConfirmPassword('targowek');
        $this->passwordEncoderService->encodePassword($user);
        $user->setEmail('targowek@courier.com');

        $district = new District();
        $district->setName('Targówek');

        $courier = new Courier();
        $courier->setUser($user);
        $courier->setDistrict($district);

        $manager->persist($courier);
        $manager->flush();
    }

    public function createCourierFromUrsus(ObjectManager $manager): void
    {
        $user = new User();
        $user-> setName('Courier Ursus');
        $user->setSurname('Ursus');
        $user->setRole('ROLE_COURIER');
        $user->setPassword('ursus');
        $user->setConfirmPassword('ursus');
        $this->passwordEncoderService->encodePassword($user);
        $user->setEmail('ursus@courier.com');

        $district = new District();
        $district->setName('Ursus');

        $courier = new Courier();
        $courier->setUser($user);
        $courier->setDistrict($district);

        $manager->persist($courier);
        $manager->flush();
    }

    public function createCourierFromUrsynow(ObjectManager $manager): void
    {
        $user = new User();
        $user-> setName('Courier Ursynów');
        $user->setSurname('Ursynow');
        $user->setRole('ROLE_COURIER');
        $user->setPassword('ursynow');
        $user->setConfirmPassword('ursynow');
        $this->passwordEncoderService->encodePassword($user);
        $user->setEmail('ursynow@courier.com');

        $district = new District();
        $district->setName('Ursynów');

        $courier = new Courier();
        $courier->setUser($user);
        $courier->setDistrict($district);

        $manager->persist($courier);
        $manager->flush();
    }

    public function createCourierFromWawer(ObjectManager $manager): void
    {
        $user = new User();
        $user-> setName('Courier Wawer');
        $user->setSurname('Wawer');
        $user->setRole('ROLE_COURIER');
        $user->setPassword('wawer');
        $user->setConfirmPassword('wawer');
        $this->passwordEncoderService->encodePassword($user);
        $user->setEmail('wawer@courier.com');

        $district = new District();
        $district->setName('Wawer');

        $courier = new Courier();
        $courier->setUser($user);
        $courier->setDistrict($district);

        $manager->persist($courier);
        $manager->flush();
    }

    public function createCourierFromWesola(ObjectManager $manager): void
    {
        $user = new User();
        $user-> setName('Courier Wesoła');
        $user->setSurname('Wesola');
        $user->setRole('ROLE_COURIER');
        $user->setPassword('wesola');
        $user->setConfirmPassword('wesola');
        $this->passwordEncoderService->encodePassword($user);
        $user->setEmail('wesola@courier.com');

        $district = new District();
        $district->setName('Wesoła');

        $courier = new Courier();
        $courier->setUser($user);
        $courier->setDistrict($district);

        $manager->persist($courier);
        $manager->flush();
    }

    public function createCourierFromWilanow(ObjectManager $manager): void
    {
        $user = new User();
        $user-> setName('Courier Wilanów');
        $user->setSurname('Wilanow');
        $user->setRole('ROLE_COURIER');
        $user->setPassword('wilanow');
        $user->setConfirmPassword('wilanow');
        $this->passwordEncoderService->encodePassword($user);
        $user->setEmail('wilanow@courier.com');

        $district = new District();
        $district->setName('Wilanów');

        $courier = new Courier();
        $courier->setUser($user);
        $courier->setDistrict($district);

        $manager->persist($courier);
        $manager->flush();
    }

    public function createCourierFromWlochy(ObjectManager $manager): void
    {
        $user = new User();
        $user-> setName('Courier Włochy');
        $user->setSurname('Wlochy');
        $user->setRole('ROLE_COURIER');
        $user->setPassword('wlochy');
        $user->setConfirmPassword('wlochy');
        $this->passwordEncoderService->encodePassword($user);
        $user->setEmail('wlochy@courier.com');

        $district = new District();
        $district->setName('Włochy');

        $courier = new Courier();
        $courier->setUser($user);
        $courier->setDistrict($district);

        $manager->persist($courier);
        $manager->flush();
    }

    public function createCourierFromWola(ObjectManager $manager): void
    {
        $user = new User();
        $user-> setName('Courier Wola');
        $user->setSurname('Wola');
        $user->setRole('ROLE_COURIER');
        $user->setPassword('wola');
        $user->setConfirmPassword('wola');
        $this->passwordEncoderService->encodePassword($user);
        $user->setEmail('wola@courier.com');

        $district = new District();
        $district->setName('Wola');

        $courier = new Courier();
        $courier->setUser($user);
        $courier->setDistrict($district);

        $manager->persist($courier);
        $manager->flush();
    }

    public function createCourierFromZoliborz(ObjectManager $manager): void
    {
        $user = new User();
        $user-> setName('Courier Żoliborz');
        $user->setSurname('Zoliborz');
        $user->setRole('ROLE_COURIER');
        $user->setPassword('zoliborz');
        $user->setConfirmPassword('zoliborz');
        $this->passwordEncoderService->encodePassword($user);
        $user->setEmail('zoliborz@courier.com');

        $district = new District();
        $district->setName('Żoliborz');

        $courier = new Courier();
        $courier->setUser($user);
        $courier->setDistrict($district);

        $manager->persist($courier);
        $manager->flush();
    }
}