<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Service\PasswordEncoderService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    private $passwordEncoderService;

    public function __construct(PasswordEncoderService $passwordEncoderService){

        $this->passwordEncoderService = $passwordEncoderService;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setName('User');
        $user->setSurname('User');
        $user->setRole('ROLE_USER');
        $user->setPassword('user');
        $user->setConfirmPassword("user");
        $this->passwordEncoderService->encodePassword($user);
        $user->setEmail('user@user.pl');

        $manager->persist($user);
        $manager->flush();

    }
}
