<?php


namespace App\Service;

use App\Repository\CourierRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class TwigService
{
    private $tokenStorage;
    private $courierRepository;

    public function __construct(TokenStorageInterface $tokenStorage,   // dostarcza mechanizm do pobierania aktualnie zalogowanego użytkownika
                                CourierRepository $courierRepository)
    {
        $this->tokenStorage = $tokenStorage;
        $this->courierRepository = $courierRepository;
    }

    private function getUserFromToken(): UserInterface
    {
        return $this->tokenStorage->getToken()->getUser();       //aktualnie zalogowany użytkownik//
    }

    public function getCourier() {
        return $this->courierRepository->findOneBy(['user' => $this->getUserFromToken()]);
    }

    public function getUser(){
        return $this->getUserFromToken();
    }

}