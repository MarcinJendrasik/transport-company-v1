<?php


namespace App\Service;

use App\Repository\CourierRepository;
use App\Repository\UserOrderRepository;
use Exception;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\User\UserInterface;

class CourierOrderService
{
    private $tokenStorage;
    private $userOrderRepository;
    private $courierRepository;

    public function __construct(TokenStorageInterface $tokenStorage,
                                CourierRepository $courierRepository,
                                UserOrderRepository $userOrderRepository)
    {
        $this->tokenStorage = $tokenStorage;
        $this->courierRepository = $courierRepository;
        $this->userOrderRepository = $userOrderRepository;
    }

    private function getUserFromToken(): UserInterface
    {
        return $this->tokenStorage->getToken()->getUser();
    }

    public function getCourierOrderAll()
    {
        $currentUser = $this->getUserFromToken();
        if ($currentUser->getRole() != "ROLE_ADMIN") {
            throw new AccessDeniedException('Unable to access this page!');
        }

        return $this->userOrderRepository->findAll();
    }

    /**
     * @param $courierId
     * @return array
     * @throws \Exception
     */
    public function getCourierOrderList($courierId)
    {
        $currentUser = $this->getUserFromToken();

        $courierOrderList = [];

        if ($currentUser->getRole() == "ROLE_ADMIN") {
            $courier = $this->courierRepository->findOneBy(["id" => $courierId]);
            $courierOrderList = $this->userOrderRepository->findBy(["courier" => $courier]);
        } else if ($currentUser->getRole() == "ROLE_COURIER") {
            $courier = $this->courierRepository->findOneBy(["user" => $currentUser]); // pobieramy kuriera takiego który jest Userem,aktualnie zalogowanym
            if ($this->isNotCourier($courier)) {
                throw new \Exception("Data inconsistency");
            }
            if ($this->isTheseOrdersAreCourierLoggedUser($courierId, $courier)) {
                $courierOrderList = $this->userOrderRepository->findBy(["courier" => $courier]);
            } else {
                throw new \Exception("Unable to access this page!");
            }
        } else {
            throw new AccessDeniedException('Unable to access this page!');
        }
        return $courierOrderList;
    }

    public function getCourierOrderByCourierAndUserOrderId($courierId, $userOrderId)
    {
        $currentUser = $this->getUserFromToken();

        $courierOrder = null;

        if ($currentUser->getRole() == "ROLE_ADMIN") {
            $courierOrder = $this->userOrderRepository->findOneBy(["courier" => $courierId, "id" => $userOrderId]);
        } else if ($currentUser->getRole() == "ROLE_COURIER") {
            $courier = $this->courierRepository->findOneBy(["user" => $currentUser]);
            if ($courier == null) {
                throw new Exception("Data inconsistency");
            }
            if ($courier->getId() == $courierId) {
                $courierOrder = $this->userOrderRepository->findOneBy(["courier" => $courier, "id" => $userOrderId]);
            } else {
                throw new Exception("Unable to access this page!");
            }
        } else {
            throw new AccessDeniedException('Unable to access this page!');
        }

        return $courierOrder;
    }

    /**
     * @param $courierId
     * @param $courier
     * @return bool
     */
    public function isTheseOrdersAreCourierLoggedUser($courierId, $courier): bool
    {
        return $courier->getId() == $courierId;
    }

    /**
     * @param $courier
     * @return bool
     */
    public function isNotCourier($courier): bool
    {
        return $courier == null;
    }
}