<?php

namespace App\Service;

use App\Repository\CourierRepository;
use App\Repository\UserOrderRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UserOrderService
{
    private $tokenStorage;
    private $userOrderRepository;
    private $courierRepository;

    public function __construct(TokenStorageInterface $tokenStorage,
                                UserOrderRepository $userOrderRepository,
                                CourierRepository $courierRepository)
    {
        $this->tokenStorage = $tokenStorage;
        $this->userOrderRepository = $userOrderRepository;
        $this->courierRepository = $courierRepository;
    }

    private function getUserFromToken(): UserInterface
    {
        return $this->tokenStorage->getToken()->getUser();

    }

    public function getUserOrderList()     //index
    {
        $currentUser = $this->getUserFromToken();
        $userOrderList = [];

        if ($currentUser->getRole() == "ROLE_ADMIN") {
            $userOrderList = $this->userOrderRepository->findAll();
        } else {
            $userOrderList = $this->userOrderRepository->findBy(["user" => $currentUser]);
        }
        return $userOrderList;            // lista wszystich zamówień
    }

    public function getUserOrderById($id)  //show
    {
        $currentUser = $this->getUserFromToken();
        $userOrder = null;

        if ($currentUser->getRole() == "ROLE_ADMIN") {
            $userOrder = $this->userOrderRepository->findOneBy(["id" => $id]);
        } else {
            $userOrder = $this->userOrderRepository->findOneBy(["id" => $id, "user" => $currentUser]);

            if ($userOrder == null) {
                throw new AccessDeniedException('Unable to access this page!');
            }
        }
        return $userOrder;              // jedno zamówienie
    }
}