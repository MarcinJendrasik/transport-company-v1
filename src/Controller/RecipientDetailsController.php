<?php

namespace App\Controller;

use App\Entity\RecipientDetails;
use App\Form\RecipientDetailsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/recipient-details")
 */
class RecipientDetailsController extends AbstractController
{
    /**
     * @Route("/", name="recipient_details_index", methods={"GET"})
     */
    public function index(): Response
    {
        $recipientDetailsList = $this->getDoctrine()
            ->getRepository(RecipientDetails::class)
            ->findAll();

        return $this->render('recipient_details/index.html.twig', [
            'recipientDetailsList' => $recipientDetailsList,
        ]);
    }

    /**
     * @Route("/new", name="recipient_details_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $recipientDetails = new RecipientDetails();
        $form = $this->createForm(RecipientDetailsType::class, $recipientDetails);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($recipientDetails);
            $entityManager->flush();

            return $this->redirectToRoute('recipient_details_index');
        }

        return $this->render('recipient_details/new.html.twig', [
            'recipientDetails' => $recipientDetails,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="recipient_details_show", methods={"GET"})
     */
    public function show(RecipientDetails $recipientDetails): Response
    {
        return $this->render('recipient_details/show.html.twig', [
            'recipientDetails' => $recipientDetails,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="recipient_details_edit", methods={"GET","POST"})
     */
    public function edit(Request $request,  RecipientDetails $recipientDetails): Response
    {
        $form = $this->createForm(RecipientDetailsType::class, $recipientDetails);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('recipient_details_index', [
                'id' => $recipientDetails->getId(),
            ]);
        }

        return $this->render('recipient_details/edit.html.twig', [
            'recipientDetails' => $recipientDetails,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="recipient_details_delete", methods={"DELETE"})
     */
    public function delete(Request $request, RecipientDetails $recipientDetails): Response
    {
        if ($this->isCsrfTokenValid('delete'.$recipientDetails->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($recipientDetails);
            $entityManager->flush();
        }

        return $this->redirectToRoute('recipient_details_index');
    }
}