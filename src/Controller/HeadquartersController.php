<?php

namespace App\Controller;

use App\Entity\Headquarters;
use App\Form\HeadquartersType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/headquarters")
 */
class HeadquartersController extends AbstractController
{
    /**
     * @Route("/", name="headquarters_index", methods={"GET"})
     */
    public function index(): Response
    {
        $headquartersList = $this->getDoctrine()
            ->getRepository(Headquarters::class)
            ->findAll();

        return $this->render('headquarters/index.html.twig', [
            'headquartersList' => $headquartersList,
        ]);
    }

    /**
     * @Route("/new", name="headquarters_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $headquarters = new Headquarters();
        $form = $this->createForm(HeadquartersType::class, $headquarters);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($headquarters);
            $entityManager->flush();

            return $this->redirectToRoute('headquarters_index');
        }

        return $this->render('headquarters/new.html.twig', [
            'headquarters' => $headquarters,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="headquarters_show", methods={"GET"})
     */
    public function show(Headquarters $headquarters): Response
    {
        return $this->render('headquarters/show.html.twig', [
            'headquarters' => $headquarters,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="headquarters_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Headquarters $headquarters): Response
    {
        $form = $this->createForm(HeadquartersType::class, $headquarters);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('headquarters_index', [
                'id' => $headquarters->getId(),
            ]);
        }

        return $this->render('headquarters/edit.html.twig', [
            'headquarters' => $headquarters,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="headquarters_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Headquarters $headquarters): Response
    {
        if ($this->isCsrfTokenValid('delete'.$headquarters->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($headquarters);
            $entityManager->flush();
        }

        return $this->redirectToRoute('headquarters_index');
    }
}

