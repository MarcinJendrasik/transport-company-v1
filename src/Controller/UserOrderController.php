<?php


namespace App\Controller;

use App\Entity\Courier;
use App\Entity\User;
use App\Entity\UserOrder;
use App\Form\UserOrderByUserType;
use App\Form\UserOrderAdminStatusType;
use App\Form\UserOrderRecipientCourierStatusType;
use App\Form\UserOrderSenderCourierStatusType;
use App\Form\UserOrderType;
use App\Service\UserOrderService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Status;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/user-order")
 */
class UserOrderController extends AbstractController
{
    private $senderCourierStatus = [
        Status::CREATED,
        Status::RECEIVED_FROM_SENDER
    ];

    private $recipientCourierStatus = [
        Status::LOCALIZE_HEADQUARTERS,
        Status::DURING_TO_RECIPIENT
    ];

    private $userOrderService;
    private $tokenStorage;

    public function __construct(UserOrderService $userOrderService, TokenStorageInterface $tokenStorage) {
        $this->userOrderService = $userOrderService;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @Route("/", name="user_order_index", methods={"GET"})
     * @throws \Exception
     */
    public function index(): Response
    {
        $userOrderList = $this->userOrderService->getUserOrderList();
        return $this->render('user_order/index.html.twig',  [
             'userOrderList' => $userOrderList,
            ]);
    }

    /**
     * @Route("/new", name="user_order_new", methods={"GET","POST"})
     * @throws \Exception
     */
    public function new(Request $request): Response
    {
        $userOrder = new UserOrder();
        $form = $this->createForm(UserOrderByUserType::class, $userOrder);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $this->prepareData($userOrder);

            $districtFromSenderDetails = $userOrder->getSenderDetails()->getDistrict();

            $courierForDistrict = $this->getDoctrine()->getRepository(Courier::class)
                ->findOneBy(['district' => $districtFromSenderDetails]);

            if ($courierForDistrict == null) {
                throw new \Exception("You should create courier for district: " . $districtFromSenderDetails);
            }

            $userOrder->setCourier($courierForDistrict);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($userOrder);
            $entityManager->flush();

            return $this->redirectToRoute('user_order_index');
        }

        return $this->render('user_order/new.html.twig',   [
            'userOrder' => $userOrder,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param UserOrder $userOrder
     * @throws \Exception
     */
    public function prepareData(UserOrder $userOrder): void
    {
        $currentUser = $this->tokenStorage->getToken()->getUser();
        $userOrder->setStatus(Status::CREATED);
        $userOrder->setCreatedAt(new \DateTime());

        $userOrder->setUser($currentUser);
    }

    /**
     * @Route("/{id}", name="user_order_show", methods={"GET"})
     * @throws \Exception
     */
    public function show($id) :Response
    {
        $userOrder = $this->userOrderService->getUserOrderById($id);

        return $this->render('user_order/show.html.twig', [
           'userOrder' => $userOrder,
        ]);
    }

    /**
     * @Route("/{id}/edit/user", name="user_order_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, $id): Response
    {
        $userOrder = $this->userOrderService->getUserOrderById($id);

        $form = $this->createForm(UserOrderByUserType::class, $userOrder);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this-> getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_order_index', [
                'id' =>  $userOrder->getId(),
            ]);
        }

        return $this->render('user_order/edit.html.twig', [
        'userOrder' => $userOrder,
        'form' => $form->createView(),
    ]);
    }

    /**
     * @Route("/{id}", name="user_order_delete", methods={"DELETE"})
     */
      public function delete(Request $request, $id): Response
      {
          $userOrder = $this->userOrderService->getUserOrderById($id);

          if ($this->isCsrfTokenValid('delete'.$userOrder->getId(), $request->request->get('_token'))){
              $entityManager = $this->getDoctrine()->getManager();
              $entityManager->remove($userOrder);
              $entityManager->flush();
          }
         return $this->redirectToRoute('user_order_index');
      }

    /**
     * @Route("/{id}/status", name="user_order_status", methods={"GET","POST"})
     */
    public function setStatus(UserOrder $userOrder, Request $request)
    {
        $currentUser = $this->tokenStorage->getToken()->getUser();
        $role = $currentUser->getRole();

        if($role == "ROLE_USER"){
            throw new AccessDeniedException('Unable to access this page!');
        }

        $currentCourier = null;
        $dataFormRequest = null;
        if($role == "ROLE_COURIER") {
            $currentCourier = $this->getDoctrine()->getRepository(Courier::class)
                ->findOneBy(['user'=> $currentUser]);
            if($currentCourier == null){
                throw new AccessDeniedException('Unable to access this page!');
            }

            $courierOrder = $this->getDoctrine()->getRepository(UserOrder::class)
                ->findBy(['id'=> $userOrder->getId(),
                    'courier'=>$currentCourier]);
            if($courierOrder == null) {
                throw new AccessDeniedException('Unable to access this page!');
            }
        }

        if($role == "ROLE_ADMIN"){
            $dataFormRequest = $request->request->get("user_order_admin_status");
            $form = $this->createForm(UserOrderAdminStatusType::class, $userOrder);
        }
        else if($role == "ROLE_COURIER" && in_array($userOrder->getStatus(), $this->senderCourierStatus)){
            $dataFormRequest = $request->request->get("user_order_sender_courier_status");
            $form = $this->createForm(UserOrderSenderCourierStatusType::class, $userOrder);
        }
        else if($role == "ROLE_COURIER" && in_array($userOrder->getStatus(), $this->recipientCourierStatus)){
            $dataFormRequest = $request->request->get("user_order_recipient_courier_status");
            $form = $this->createForm(UserOrderRecipientCourierStatusType::class, $userOrder);
        }
        else{
            throw new AccessDeniedException('Unable to access this page!');
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $districtFromRecipientDetails = $userOrder->getSenderDetails()->getDistrict();
            if($userOrder->getCourier()->getDistrict() != $districtFromRecipientDetails && in_array($dataFormRequest["status"], $this->senderCourierStatus)){               //w bloku kodu if stworzylismy logike,która odpowiada za zmiane kuriera 1 na kureira 2
                $courierForDistrict = $this->getDoctrine()->getRepository(Courier::class)
                    ->findOneBy(['district' => $districtFromRecipientDetails]);
                $userOrder->setCourier($courierForDistrict);
            }

            $districtFromRecipientDetails = $userOrder->getRecipientDetails()->getDistrict();
            if($userOrder->getCourier()->getDistrict() != $districtFromRecipientDetails && in_array($dataFormRequest["status"], $this->recipientCourierStatus)){               //w bloku kodu if stworzylismy logike,która odpowiada za zmiane kuriera 1 na kureira 2
                $courierForDistrict = $this->getDoctrine()->getRepository(Courier::class)
                    ->findOneBy(['district' => $districtFromRecipientDetails]);
                $userOrder->setCourier($courierForDistrict);
            }

            if(Status::PROVIDED == $dataFormRequest["status"]){
                $userOrder->setCourier(null);
            }

            $this->getDoctrine()->getManager()->flush();

            if($role == "ROLE_ADMIN"){
                return $this->redirectToRoute('courier_order_index');
            }
            else{
                return $this->redirectToRoute('courier_order_index_by_courier', [
                    'courierId' =>  $currentCourier->getId()
                ]);
            }
        }

        return $this->render('user_order/status.html.twig', [
            'userOrder' => $userOrder,
            'form' => $form->createView()
        ]);
    }
}

