<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Service\PasswordEncoderService;
use App\Service\UserService;
use Exception;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    private $userService;
    private $passwordEncoderService;
    private $passwordConfirmedService;

    public function __construct(UserService $userService,                // konstruktor o nazwie UserService ma taka sama nazwe jak klasa
                                PasswordEncoderService $passwordEncoderService) {  //,a w nim znajdują się zmienne wyjściowe - $userService

        $this->userService = $userService;
        $this->passwordEncoderService = $passwordEncoderService;
    }

    /**
     * @Route("/", name="user_index", methods={"GET"})
     */
    public function index(): Response
    {
        $users = $this->userService->getUsers();

        return $this->render('user/index.html.twig', [
            'users' => $users,
        ]);
    }


    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     * @throws Exception
     */
    public function new(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);       //  odwołanie do classy UserType
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user->setRole("ROLE_USER");
            $user = $this->passwordEncoderService->encodePassword($user);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_index');   //przekierowane z create new user do user list
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    //        throw new AccessDeniedException('Unable to access this page!');

//    public function test()
//    {
//        $a = new UserController(new UserService(), new PasswordEncoderService());
//        $a->index();
//    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     */
    public function show($id): Response
    {
        $user = $this->userService->getUserById($id);
        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        $user = $this->passwordEncoderService->encodePassword($user);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index', [
                'id' => $user->getId(),
            ]);
        }
        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }
}
