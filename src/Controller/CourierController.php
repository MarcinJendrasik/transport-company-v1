<?php

namespace App\Controller;

use App\Entity\Courier;
use App\Entity\User;
use App\Form\CourierType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/courier")
 */
class CourierController extends AbstractController
{

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder) {
        $this->passwordEncoder = $passwordEncoder;
    }
    /**
     * @Route("/", name="courier_index", methods={"GET"})
     */
    public function index(): Response
    {
        $couriers = $this->getDoctrine()
            ->getRepository(Courier::class)
            ->findAll();

        return $this->render('courier/index.html.twig', [
            'couriers' => $couriers,
        ]);
    }

    /**
     * @Route("/new", name="courier_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $courier = new Courier();
        $form = $this->createForm(CourierType::class, $courier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {      // jest przesłany i jest ważny
            $entityManager = $this->getDoctrine()->getManager();

            $user = $courier->getUser();
            $user->setRole("ROLE_COURIER");
            $newEncodedPassword = $this->passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($newEncodedPassword);

            $entityManager->persist($courier);
            $entityManager->flush();

            return $this->redirectToRoute('courier_index');
        }

        return $this->render('courier/new.html.twig', [
            'courier' => $courier,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="courier_show", methods={"GET"})
     */
    public function show(Courier $courier): Response
    {
        return $this->render('courier/show.html.twig', [
            'courier' => $courier,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="courier_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Courier $courier): Response
    {
        $form = $this->createForm(CourierType::class, $courier);
        $form->handleRequest($request);

        $user = $courier->getUser();
        $newEncodedPassword = $this->passwordEncoder->encodePassword($user, $user->getPassword());
        $user->setPassword($newEncodedPassword);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('courier_index', [
                'id' => $courier->getId(),
            ]);
        }

        return $this->render('courier/edit.html.twig', [
            'courier' => $courier,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="courier_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Courier $courier): Response
    {
        if ($this->isCsrfTokenValid('delete'.$courier->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($courier);
            $entityManager->flush();
        }

        return $this->redirectToRoute('courier_index');
    }
}