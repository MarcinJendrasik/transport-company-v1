<?php

namespace App\Controller;

use App\Entity\SenderDetails;
use App\Form\SenderDetailsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/sender-details")
 */
class SenderDetailsController extends AbstractController
{
    /**
     * @Route("/", name="sender_details_index", methods={"GET"})
     */
   public function index(): Response
   {
       $senderDetailsList = $this->getDoctrine()
           ->getRepository(SenderDetails::class)
           ->findAll();

       return $this->render('sender_details/index.html.twig',  [
           'senderDetailsList' => $senderDetailsList,
       ]);
   }

   /**
    * @Route("/new", name="sender_details_new", methods={"GET","POST"})
    */
   public function new(Request $request): Response
   {
       $senderDetails = new SenderDetails();
       $form = $this->createForm(SenderDetailsType::class, $senderDetails);
       $form->handleRequest($request);

       if ($form->isSubmitted() && $form->isValid()) {
           $entityManager = $this->getDoctrine()->getManager();
           $entityManager->persist($senderDetails);
           $entityManager->flush();

           return $this->redirectToRoute('sender_details_index');
       }

       return $this->render('sender_details/new.html.twig',  [
           'senderDetails' => $senderDetails,
           'form' => $form->createView(),
       ]);
   }

    /**
     * @Route("/{id}", name="sender_details_show", methods={"GET"})
     */
   public function show(SenderDetails $senderDetails) : Response
   {
       return $this->render('sender_details/show.html.twig', [
           'senderDetails' => $senderDetails,
           ]);
   }

    /**
     * @Route("/{id}/edit", name="sender_details_edit", methods={"GET","POST"})
     */

    public function edit(Request $request,  SenderDetails $senderDetails): Response
    {
        $form = $this->createForm(SenderDetailsType::class, $senderDetails);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sender_details_index', [
                'id' => $senderDetails->getId(),
            ]);
        }

        return $this->render('recipient_details/edit.html.twig', [
            'recipientDetails' => $senderDetails,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sender_details_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SenderDetails $senderDetails): Response
    {
        if ($this->isCsrfTokenValid('delete'.$senderDetails->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($senderDetails);
            $entityManager->flush();
        }

        return $this->redirectToRoute('sender_details_index');
    }
}