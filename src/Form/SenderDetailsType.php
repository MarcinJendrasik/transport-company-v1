<?php


namespace App\Form;

use App\Entity\District;
use App\Entity\SenderDetails;
use App\Model\Status;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SenderDetailsType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('surname')
            ->add('city')
            ->add('street')
            ->add('houseNumber')
            ->add('apartmentNumber')
            ->add('email')
            ->add("district",  EntityType::class, [
                'class' => District::class,
                'choice_label' => 'name'
            ])
            ->add('phoneNumber');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SenderDetails::class,
        ]);
    }
}