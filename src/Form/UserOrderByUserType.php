<?php


namespace App\Form;


use App\Entity\Headquarters;
use App\Entity\Package;
use App\Entity\RecipientDetails;
use App\Entity\SenderDetails;
use App\Entity\UserOrder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserOrderByUserType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number')
            ->add('description')
            ->add('comments')
            ->add('headquarters', EntityType::class, [
                'class' => Headquarters::class,
                'choice_label' => 'name'
            ])
            ->add('package', PackageType::class, [
                "data_class" => Package::class
            ])
            ->add('senderDetails', SenderDetailsType::class,[
                "data_class" => SenderDetails::class
            ])
            ->add('recipientDetails', RecipientDetailsType::class, [
                "data_class" => RecipientDetails::class
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserOrder::class
        ]);
    }
}