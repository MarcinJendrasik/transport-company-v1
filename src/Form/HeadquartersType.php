<?php

namespace App\Form;

use App\Entity\Headquarters;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class HeadquartersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('city')
            ->add('street')
            ->add('houseNumber')
            ->add('apartmentNumber')
            ->add('voivodeship')
            ->add('zipCode')
            ->add('email')
            ->add('phoneNumber')
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Headquarters::class,
        ]);
    }
}