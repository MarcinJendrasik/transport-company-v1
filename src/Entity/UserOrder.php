<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserOrder
 *
 * @ORM\Table(name="user_order", indexes={@ORM\Index(name="fk_order_recipient_details1_idx", columns={"recipient_details_id"}), @ORM\Index(name="fk_order_package1_idx", columns={"package_id"}), @ORM\Index(name="fk_order_user1_idx", columns={"user_id"}), @ORM\Index(name="fk_order_sender_details1_idx", columns={"sender_details_id"}), @ORM\Index(name="fk_order_headquarters1_idx", columns={"headquarters_id"})})
 * @ORM\Entity
 */
class UserOrder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="number", type="string", length=255, nullable=true)
     */
    private $number;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comments", type="string", length=255, nullable=true)
     */
    private $comments;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @var Package
     *
     * @ORM\ManyToOne(targetEntity="Package", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="package_id", referencedColumnName="id")
     * })
     */
    private $package;

    /**
     * @var RecipientDetails
     *
     * @ORM\ManyToOne(targetEntity="RecipientDetails", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="recipient_details_id", referencedColumnName="id")
     * })
     */
    private $recipientDetails;


    /**
     *
     * @ORM\ManyToOne(targetEntity="User", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var SenderDetails
     *
     * @ORM\ManyToOne(targetEntity="SenderDetails", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sender_details_id", referencedColumnName="id")
     * })
     */
    private $senderDetails;

    /**
     * @var Headquarters
     *
     * @ORM\ManyToOne(targetEntity="Headquarters")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="headquarters_id", referencedColumnName="id")
     * })
     */
    private $headquarters;

    /**
     * @var Courier
     *
     * @ORM\ManyToOne(targetEntity="Courier")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="courier_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $courier;
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * @param string|null $number
     */
    public function setNumber(?string $number): void
    {
        $this->number = $number;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getComments(): ?string
    {
        return $this->comments;
    }

    /**
     * @param string|null $comments
     */
    public function setComments(?string $comments): void
    {
        $this->comments = $comments;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime|null $createdAt
     */
    public function setCreatedAt(?\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }


    /**
     * @return Headquarters
     */
    public function getHeadquarters()
    {
        return $this->headquarters;
    }

    /**
     * @param Headquarters $headquarters
     */
    public function setHeadquarters(Headquarters $headquarters): void
    {
        $this->headquarters = $headquarters;
    }


    /**
     * @return Package
     */
    public function getPackage()
    {
        return $this->package;
    }

    /**
     * @param Package $package
     */
    public function setPackage(Package $package): void
    {
        $this->package = $package;
    }

    /**
     * @return SenderDetails
     */
    public function getSenderDetails()
    {
        return $this->senderDetails;
    }

    /**
     * @param SenderDetails $senderDetails
     */
    public function setSenderDetails(SenderDetails $senderDetails): void
    {
        $this->senderDetails = $senderDetails;
    }

    /**
     * @return RecipientDetails
     */
    public function getRecipientDetails()
    {
        return $this->recipientDetails;
    }

    /**
     * @param RecipientDetails $recipientDetails
     */
    public function setRecipientDetails(RecipientDetails $recipientDetails): void
    {
        $this->recipientDetails = $recipientDetails;
    }



    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     */
    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return Courier
     */
    public function getCourier()
    {
        return $this->courier;
    }

    public function setCourier($courier): void
    {
        $this->courier = $courier;
    }
}
