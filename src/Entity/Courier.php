<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Courier
 *
 * @ORM\Table(name="courier", indexes={@ORM\Index(name="fk_courier_user1_idx", columns={"user_id"})})
 * @ORM\Entity
 */
class Courier
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="User", cascade={"persist"})
     */
    private $user;

    /**
     *
     * @ORM\ManyToOne(targetEntity="District", cascade={"persist"}, )
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="district_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $district;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param UserInterface $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param mixed $district
     */
    public function setDistrict(District $district): void
    {
        $this->district = $district;
    }
}
