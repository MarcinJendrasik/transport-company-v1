-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema transport_company
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema transport_company
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `transport_company` DEFAULT CHARACTER SET utf8 ;
USE `transport_company` ;

-- -----------------------------------------------------
-- Table `transport_company`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `transport_company`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL,
  `surname` VARCHAR(255) NULL,
  `role` VARCHAR(255) NULL,
  `password` VARCHAR(255) NULL,
  `enable` TINYINT NULL,
  `email` VARCHAR(255) NULL,
  `phone_number` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `transport_company`.`address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `transport_company`.`address` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `city` VARCHAR(255) NULL,
  `street` VARCHAR(255) NULL,
  `house_number` VARCHAR(255) NULL,
  `apartment_number` VARCHAR(255) NULL,
  `voivodeship` VARCHAR(255) NULL,
  `zip_code` VARCHAR(255) NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_address_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_address_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `transport_company`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `transport_company`.`headquarters`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `transport_company`.`headquarters` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL,
  `city` VARCHAR(255) NULL,
  `street` VARCHAR(255) NULL,
  `house_number` VARCHAR(255) NULL,
  `apartment_number` VARCHAR(255) NULL,
  `voivodeship` VARCHAR(255) NULL,
  `zip_code` VARCHAR(255) NULL,
  `email` VARCHAR(255) NULL,
  `phone_number` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `transport_company`.`package`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `transport_company`.`package` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `weight` INT NULL,
  `width` INT NULL,
  `height` INT NULL,
  `length` INT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `transport_company`.`sender_details`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `transport_company`.`sender_details` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL,
  `surrname` VARCHAR(255) NULL,
  `city` VARCHAR(255) NULL,
  `street` VARCHAR(255) NULL,
  `house_number` VARCHAR(255) NULL,
  `apartment_number` VARCHAR(255) NULL,
  `phone_number` VARCHAR(255) NULL,
  `email` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `transport_company`.`recipient_details`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `transport_company`.`recipient_details` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL,
  `surrname` VARCHAR(255) NULL,
  `city` VARCHAR(255) NULL,
  `street` VARCHAR(255) NULL,
  `house_number` VARCHAR(255) NULL,
  `apartment_number` VARCHAR(255) NULL,
  `phone_number` VARCHAR(255) NULL,
  `email` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `transport_company`.`courier`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `transport_company`.`courier` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_courier_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_courier_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `transport_company`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `transport_company`.`order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `transport_company`.`order` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `number` VARCHAR(255) NULL,
  `description` VARCHAR(255) NULL,
  `comments` VARCHAR(255) NULL,
  `user_id` INT NULL,
  `headquarters_id` INT NULL,
  `created_at` DATETIME NULL,
  `package_id` INT NULL,
  `sender_details_id` INT NULL,
  `recipient_details_id` INT NULL,
  `status` VARCHAR(255) NULL,
  `courier_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_order_user1_idx` (`user_id` ASC),
  INDEX `fk_order_headquarters1_idx` (`headquarters_id` ASC),
  INDEX `fk_order_package1_idx` (`package_id` ASC),
  INDEX `fk_order_sender_details1_idx` (`sender_details_id` ASC),
  INDEX `fk_order_recipient_details1_idx` (`recipient_details_id` ASC),
  INDEX `fk_order_courier1_idx` (`courier_id` ASC),
  CONSTRAINT `fk_order_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `transport_company`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_headquarters1`
    FOREIGN KEY (`headquarters_id`)
    REFERENCES `transport_company`.`headquarters` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_package1`
    FOREIGN KEY (`package_id`)
    REFERENCES `transport_company`.`package` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_sender_details1`
    FOREIGN KEY (`sender_details_id`)
    REFERENCES `transport_company`.`sender_details` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_recipient_details1`
    FOREIGN KEY (`recipient_details_id`)
    REFERENCES `transport_company`.`recipient_details` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_courier1`
    FOREIGN KEY (`courier_id`)
    REFERENCES `transport_company`.`courier` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
